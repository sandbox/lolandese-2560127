<?php
/**
 * @file
 * Delicious feeds parser class.
 */

/**
 * Class definition for Delicious Parser.
 */
class FeedsDeliciousParser extends FeedsParser {

  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $delicious_feed = $fetcher_result->getRaw();
    $result = new FeedsParserResult();

    $json = json_decode($delicious_feed);

    $items = array();
    foreach ($json as $entry) {
      $items[] = array(
        'user' => (isset($entry->a) ? $entry->a : ''),
        'title' => (isset($entry->d) ? $entry->d : ''),
        'comment' => (isset($entry->n) ? $entry->n : ''),
        'url' => (isset($entry->u) ? $entry->u : ''),
        'tags' => (isset($entry->t) ? $entry->t : ''),
        'date' => (isset($entry->dt) ? $entry->dt : ''),
      );
    }
    $result->items = $items;
    return $result;
  }

  public function getMappingSources() {
    return parent::getMappingSources() + array(
      'user' => array(
        'name' => t('User'),
        'description' => t('User that bookmarked.'),
      ),
      'title' => array(
        'name' => t('Title'),
        'description' => t('Page title.'),
      ),
      'comment' => array(
        'name' => t('Comment'),
        'description' => t('Text added by the bookmarking user.'),
      ),
      'url' => array(
        'name' => t('URL'),
        'description' => t('Link to the bookmarked page'),
      ),
      'tags' => array(
        'name' => t('Tags'),
        'description' => t('Categories added by the bookmarking user.'),
      ),
      'date' => array(
        'name' => t('Date bookmarked'),
        'description' => t('ISO 8601 format (e.g. 2015-01-18T21:03:59Z).'),
      ),
    );
  }
}
