Below is an example how to set up a Delicious feed.

Besides this module enable the sub-module Feeds UI (feeds_ui, part of Feeds) and
its dependencies.

Create at 'admin/structure/types/add' a content type 'Delicious feed' (Machine
name: delicious_feed). You can delete the unused 'Body' field.

Create a content type 'Delicious feed item' (Machine name: delicious_feed_item)
with the following extra fields: - User (field_user) - Field type: Text field -
URL (field_url) - Field type: Text field - Tags (field_categories) - Field type:
Text field - Number of values: Unlimited Leave the default added 'Title' and
'Body' field untouched.

Create at 'admin/structure/feeds/import' a new 'Feeds importer' by copy-pasting
the text of the file 'example_importer.txt' that is in the same folder as this
README.txt.

Create a feed at 'node/add/delicious-user-feed'. A title like 'Delicious from
lolandese tagged D8' will do but is not relevant. Use a feed URL like:

  http://feeds.delicious.com/v2/json/lolandese/D8?count=100

More info about possible URLs for Delicious RSS Feeds at:

  https://delicious.com/rss

After saving you should get a status message like 'Created 82 nodes'. Go to
'admin/content' to check.
